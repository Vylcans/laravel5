@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Users</div>

				<div class="panel-body">
				    @foreach($users as $user)
                        <div>
                            <div>
                                <b>{{ $user->name }}</b>
                                @if($user->isFollowed)
                                    <span class="label label-default">Followed</span>
                                @else
                                    <a href="{{ URL::route('followUser', ['id' => $user->id]) }}" class="btn btn-default">Follow</a>
                                @endif
                            </div>
                            @if(!$user->tweets->isEmpty())
                                <p>
                                    {{ $user->tweets->last()->content }}
                                </p>
                            @endif
                        </div>
                    @endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
