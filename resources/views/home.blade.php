@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					<h3>Tweet</h3>
					{!! Form::open(['url' => 'tweet']) !!}
					<div>
					    {!! Form::textarea('tweet') !!}
					</div>
                    <div>
                        {!! Form::submit() !!}
                    </div>

					{!! Form::close() !!}
				</div>
				<div>
				    @foreach($tweets as $tweet)
				        <div>
				            <b>{{ $tweet->user->name }}</b>
				        </div>
				        <div>
				            {{ date("d.m.Y H:i:s",strtotime($tweet->created_at)) }}
				        </div>
				        <div>
				            {{ $tweet->content }}
				        </div>
				        <br />
				    @endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
