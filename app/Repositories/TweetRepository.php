<?php
/**
 * Created by PhpStorm.
 * User: vylcans
 * Date: 15.28.2
 * Time: 11:03
 */

namespace App\Repositories;


use App\Constants\FormConstants;
use App\Models\Tweet;

class TweetRepository
{
    /**
     * Creates new tweet
     */
    public static function createTweet()
    {
        $message = \Input::get(FormConstants::TWEET);
        $tweet = new Tweet();
        $tweet->content = $message;
        $tweet->user_id = \Auth::user()->id;
        $tweet->save();
    }


    public static function getUserTweets()
    {
        return Tweet::where('user_id', '=', \Auth::user()->id)
            ->orWhereIn('user_id', \Auth::user()->followingIdList())
            ->orderBy('id', 'desc')->get();
    }
}
