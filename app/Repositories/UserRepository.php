<?php
/**
 * Created by PhpStorm.
 * User: vylcans
 * Date: 15.28.2
 * Time: 11:03
 */

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    /**
     * @return array
     */
    public static function getAllUsersExceptCurrent()
    {
        $users = User::where('id', '<>', \Auth::user()->id)->get();

        /**
         * @var User $user
         */
        foreach ($users as $user) {
            self::makeFollowStatus($user);
        }

        return $users;
    }

    /**
     * Checks if current users follows given user
     * @param User $user
     */
    private static function makeFollowStatus(User $user)
    {
        $followerIdList = $user->followerIdList();

        if (in_array(\Auth::user()->id, $followerIdList)) {
            $user->isFollowed = true;
        } else {
            $user->isFollowed = false;
        }
    }

    /**
     * @param User $userToFollow
     */
    public static function followUser(User $userToFollow)
    {
        $currentUser = \Auth::user();
        $currentUser->followings()->attach($userToFollow->id);
    }
}
