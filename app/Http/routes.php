<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::post('/tweet', [
    'as' => 'createTweet',
    'uses' => 'TweetController@create'
]);

Route::get('/users', [
    'as' => 'users',
    'uses' => 'HomeController@usersList'
]);

Route::get('/follow/{id}', [
    'as' => 'followUser',
    'uses' => 'HomeController@follow'
]);
