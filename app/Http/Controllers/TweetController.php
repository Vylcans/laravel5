<?php
/**
 * Created by PhpStorm.
 * User: vylcans
 * Date: 15.28.2
 * Time: 11:22
 */

namespace App\Http\Controllers;

use App\Repositories\TweetRepository;

class TweetController extends Controller
{
    public function create()
    {
        TweetRepository::createTweet();

        return \Redirect::to('home');
    }

}
