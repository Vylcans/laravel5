<?php namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\TweetRepository;
use App\Repositories\UserRepository;

class HomeController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        $tweets = TweetRepository::getUserTweets();

        return view(
            'home',
            [
                'tweets' => $tweets
            ]
        );
    }

    public function usersList()
    {
        $users = UserRepository::getAllUsersExceptCurrent();

        return view(
            'users',
            [
                'users' => $users
            ]
        );
    }

    public function follow($id)
    {
        $userToFollow = User::find($id);
        UserRepository::followUser($userToFollow);

        return \Redirect::to('users');
    }
}
