<?php
/**
 * Created by PhpStorm.
 * User: vylcans
 * Date: 15.28.2
 * Time: 10:58
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{

    /**
     * Relation to user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
